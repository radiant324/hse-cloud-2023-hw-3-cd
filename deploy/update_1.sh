#!/usr/bin/env bash
# terraform apply
terraform output -json > terraform.out
j2 -f json ansible-inventory.j2 terraform.out -o ansible_inventory
ansible-playbook update_compute_1.yml --extra-vars="@release.json" --extra-vars="token=$(cat oauth.token)"
